//
//  NSError+Category.h
//  XCRIOS
//
//  Created by ShaY on 2017/4/7.
//  Copyright © 2017年 YaTang Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const int kCodeOK;
extern const int kInnerErrorCode;
extern const int kCancelCode;
extern const int kDataFormatErrorCode;
extern const int kNetWorkErrorCode;
extern const int kLogicErrorCode;
extern const int kNoDataErrorCode;
extern const int kAsyncCanelErrorCode;
extern const int kJsonFormatErrorCode;

@interface NSError (Category)

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code;

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description;

+ (instancetype)errorWithDomain:(NSString *)domain
                           code:(NSInteger)code
                    description:(NSString *)description
                         reason:(NSString *)reason;

@end
