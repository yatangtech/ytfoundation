//
//  NSFileManager+Category.m
//  XCRIOS
//
//  Created by clee on 2017/4/1.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import "NSFileManager+Category.h"

@implementation NSFileManager (Category)

+ (NSString *)documentPath:(NSString *)subPath
{
    NSString *dir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    if (subPath)
        return [dir stringByAppendingFormat:@"/%@",subPath];
    else return dir;
}

+ (NSString *)cachePath:(NSString *)subPath
{
    NSString *dir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    if (subPath) return [dir stringByAppendingFormat:@"/%@",subPath];
    else return dir;
}

+ (NSString *)libPath:(NSString *)subPath
{
    NSString *dir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    if (subPath) return [dir stringByAppendingFormat:@"/%@",subPath];
    else return dir;
}
+ (NSString *)tempPath:(NSString *)subPath
{
    NSString *dir = NSTemporaryDirectory();
    if (subPath) return [dir stringByAppendingFormat:@"%@",subPath];
    else return dir;
}

@end
