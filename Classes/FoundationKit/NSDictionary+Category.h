//
//  NSDictionary+Category.h
//  XCRIOS
//
//  Created by caodaxun on 17/3/22.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Category)

- (NSString *)convertToString;

@end
