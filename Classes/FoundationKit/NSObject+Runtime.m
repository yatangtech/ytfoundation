//
//  NSObject+Runtime.m
//  XCRIOS
//
//  Created by LiXiang on 2017/5/16.
//  Copyright © 2017年 YaTang Technology. All rights reserved.
//

#import "NSObject+Runtime.h"
#import <objc/runtime.h>

@implementation NSObject (Runtime)

+ (void)printClassMethodList {
    NSLog(@"\\*** 以下是 %@ 的类方法 ***\\", NSStringFromClass(self));
    unsigned int count = 0;
    Method *methodList = class_copyMethodList(self, &count);
    for (int i = 0; i < count; i++) {
        // 获取方法
        Method method = methodList[i];
        // 获取方法名
        SEL sel = method_getName(method);
        NSLog(@"%@", NSStringFromSelector(sel));
    }
}

@end
