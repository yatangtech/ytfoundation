//
//  NSFileManager+Category.h
//  XCRIOS
//
//  Created by clee on 2017/4/1.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Category)

/**
 目录/Documents
 
 @param subPath 子目录
 @return 完整路径
 */
+ (NSString *)documentPath:(NSString *)subPath;
/**
 目录/Library/Cache

 @param subPath 子目录
 @return 完整路径
 */
+ (NSString *)cachePath:(NSString *)subPath;

/**
 目录/Library

 @param subPath 子目录
 @return 完整路径   
 */
+ (NSString *)libPath:(NSString *)subPath;

/**
 目录/tmp

 @param subPath 子目录
 @return 完整路径   
 */
+ (NSString *)tempPath:(NSString *)subPath;

@end
