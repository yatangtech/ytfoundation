//
//  NSString+Category.m
//  XCRIOS
//
//  Created by caodaxun on 17/3/20.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import "NSString+Category.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

static NSDateFormatter *formatter;

@implementation NSString (Category)

- (NSString *)encryptMD5 {
    
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return [output uppercaseString];
}

- (NSString *)encryptDES:(NSString *)key {
    
    const Byte iv[] = {1,2,3,4,5,6,7,8};

    NSString *ciphertext = nil;
    NSData *textData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSUInteger dataLength = [textData length];
    unsigned char buffer[1024];
    memset(buffer, 0, sizeof(char));
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding,
                                          [key UTF8String], kCCKeySizeDES,
                                          iv,
                                          [textData bytes], dataLength,
                                          buffer, 1024,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        NSData *data = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesEncrypted];
        ciphertext = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedData:data options:0]
                                           encoding:NSUTF8StringEncoding];;
    }
    return ciphertext;
}

- (NSString *)decryptDES:(NSString *)key {
    
    return @"";
}

- (NSString *)priceFormat {
    
    NSString *str = [NSString stringWithFormat:@"%.2f", [self doubleValue]];
    
    CGFloat value = [str doubleValue];
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    format.numberStyle = NSNumberFormatterDecimalStyle;
    format.maximumFractionDigits = 2;
    format.minimumFractionDigits = 2;
    return [format stringFromNumber:@(value)];
}

- (NSDate *)date {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    //    formatter.dateStyle = NSDateFormatterShortStyle;
    return [formatter dateFromString:self];
}

+ (NSString *)timeDescriptionWithSeconds:(NSInteger)seconds {
    NSInteger hour = seconds / 3600;
    seconds = seconds % 3600;
    NSInteger minute = seconds / 60;
    seconds = seconds % 60;
    NSMutableString *desc = [NSMutableString string];
    if (hour > 0) {
        [desc appendFormat:@"%ld小时", hour];
        [desc appendFormat:@"%ld分", minute];
    } else {
        if (minute > 0) {
            [desc appendFormat:@"%ld分", minute];
        }
    }
    [desc appendFormat:@"%ld秒", seconds];
    return desc;
}

+ (NSString*)hexStringWithData:(unsigned char*)data ofLength:(NSUInteger)len
{
    NSMutableString *tmp = [NSMutableString string];
    for (NSUInteger i=0; i<len; i++)
        [tmp appendFormat:@"%02x", data[i]];
    return [NSString stringWithString:tmp];
}

+(NSString *)parseByte2HexString:(Byte *)bytes
{
    NSMutableString *hexStr = [[NSMutableString alloc]init];
    int i = 0;
    if(bytes)
    {
        while (bytes[i] != '\0')
        {
            NSString *hexByte = [NSString stringWithFormat:@"%x",bytes[i] & 0xff];///16进制数
            if([hexByte length]==1)
                [hexStr appendFormat:@"0%@", hexByte];
            else
                [hexStr appendFormat:@"%@", hexByte];
            
            i++;
        }
    }
    NSLog(@"bytes 的16进制数为:%@",hexStr);
    return hexStr;
}



/** 经纬度转换成度分秒格式 */
- (NSString *)stringWithCoordinateString
{
    /** 将经度或纬度整数部分提取出来 */
    int latNumber = [self intValue];
    
    /** 取出小数点后面两位(为转化成'分'做准备) */
    NSArray *array = [self componentsSeparatedByString:@"."];
    /** 小数点后面部分 */
    NSString *lastCompnetString = [array lastObject];
    
    /** 拼接字字符串(将字符串转化为0.xxxx形式) */
    NSString *str1 = [NSString stringWithFormat:@"0.%@", lastCompnetString];
    
    /** 将字符串转换成float类型以便计算 */
    float minuteNum = [str1 floatValue];
    
    /** 将小数点后数字转化为'分'(minuteNum * 60) */
    float minuteNum1 = minuteNum * 60;
    
    /** 将转化后的float类型转化为字符串类型 */
    NSString *latStr = [NSString stringWithFormat:@"%f", minuteNum1];
    
    /** 取整数部分即为纬度或经度'分' */
    int latMinute = [latStr intValue];
    
    /** 将经度或纬度字符串合并为(xx°xx')形式 */
    NSString *string = [NSString stringWithFormat:@"%d°%d'", latNumber, latMinute];
    
    return string;
}
@end
