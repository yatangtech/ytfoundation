//
//  NSDate+Category.h
//  XCRIOS
//
//  Created by caodaxun on 17/3/23.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Category)

+ (NSDate *)yesterday;

/**
 指定日期前一天
 */
+ (NSDate *)yesterdayForDate:(NSDate *)date;

/**
 * 2017-07-23 18:12:40
 */
- (NSString *)stringForLongFormat;

/**
 * 2017-07-23
 */
- (NSString *)stringForShortFormat;

/**
 * 自定义格式
 */
- (NSString *)stringFor:(NSString *)formatString;

/**
 *  是不是今天
 */
- (BOOL)isToday;

/**
 *  字符串表示今天的日期
 */
+ (NSString *)todayOfString;

/**
 *  字符串表示昨天的日期
 */
+ (NSString *)yesterdayOfString;

/**
 *  这个月的第一天的字符串表示
 */
+ (NSString *)firstDayOfCurrentMonth;

/**
 *  星期几
 */
- (NSInteger)weekDay;

/**
 *  星期几的字符串表示
 */
- (NSString *)weekDayOfString;

@end
