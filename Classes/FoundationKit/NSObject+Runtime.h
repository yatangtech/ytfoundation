//
//  NSObject+Runtime.h
//  XCRIOS
//
//  Created by LiXiang on 2017/5/16.
//  Copyright © 2017年 YaTang Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Runtime)

+ (void)printClassMethodList;

@end
