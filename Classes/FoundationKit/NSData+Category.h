//
//  NSData+Category.h
//  XCRIOS
//
//  Created by caodaxun on 17/3/21.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Category)

- (NSString *)hexString;

@end
