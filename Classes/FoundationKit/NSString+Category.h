//
//  NSString+Category.h
//  XCRIOS
//
//  Created by caodaxun on 17/3/20.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

- (NSString *)encryptMD5;

- (NSString *)encryptDES:(NSString *)key;

- (NSString *)decryptDES:(NSString *)key;


/**
 格式:1,234,567.837
 */
- (NSString *)priceFormat;

- (NSDate *)date;

- (NSString *)stringWithCoordinateString;

+ (NSString *)timeDescriptionWithSeconds:(NSInteger)seconds;

+ (NSString*)hexStringWithData:(unsigned char*)data ofLength:(NSUInteger)len;

+(NSString *)parseByte2HexString:(Byte *)bytes;


@end
