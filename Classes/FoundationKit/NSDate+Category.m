//
//  NSDate+Category.m
//  XCRIOS
//
//  Created by caodaxun on 17/3/23.
//  Copyright © 2017年 caodaxun. All rights reserved.
//

#import "NSDate+Category.h"

static NSDateFormatter *formatter;
static NSCalendar *calendar;

@implementation NSDate (Category)

+ (NSDate *)yesterday {
    return [NSDate dateWithTimeIntervalSinceNow:-(60*60*24)];
}

+ (NSDate *)yesterdayForDate:(NSDate *)date {
    return [NSDate dateWithTimeInterval:-(60*60*24) sinceDate:date];
}

- (NSString *)stringForLongFormat {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
    }
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [formatter stringFromDate:self];
}

- (NSString *)stringForShortFormat {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
    }
    formatter.dateFormat = @"yyyy-MM-dd";
    return [formatter stringFromDate:self];
}

- (NSString *)stringFor:(NSString *)formatString {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
    }
    formatter.dateFormat = formatString;
    return [formatter stringFromDate:self];
}

- (BOOL)isToday {
    if (calendar == nil) {
        calendar = [NSCalendar currentCalendar];
    }
    
    return [calendar isDateInToday:self];
}

+ (NSString *)todayOfString {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
    }
    formatter.dateFormat = @"yyyy-MM-dd";
    return [formatter stringFromDate:[NSDate date]];
}

+ (NSString *)yesterdayOfString {
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
    }
    formatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yesterday = [self yesterday];
    return [formatter stringFromDate:yesterday];
}

+ (NSString *)firstDayOfCurrentMonth {
    if (calendar == nil) {
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    comps.day = 1;
    NSDate *theDate = [calendar dateFromComponents:comps];
    return [theDate stringForShortFormat];
}

- (NSInteger)weekDay {
    if (calendar == nil) {
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    NSDateComponents *comps = [calendar components:NSCalendarUnitWeekday fromDate:self];
    return comps.weekday;
}

- (NSString *)weekDayOfString {
    NSInteger weekDay = [self weekDay];
    switch (weekDay) {
        case 1:
            return @"日";
        case 2:
            return @"一";
        case 3:
            return @"二";
        case 4:
            return @"三";
        case 5:
            return @"四";
        case 6:
            return @"五";
        default:
            return @"六";
    }
}

@end
