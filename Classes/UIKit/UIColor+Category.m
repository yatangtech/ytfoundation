//
//  UIColor+Category.m
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/8.
//
//

#import "UIColor+Category.h"

@implementation UIColor (Category)

#pragma mark - Application Color

+ (UIColor *)redTextColor {
    return [UIColor colorWithRed: 218.0 / 255.0f green:37.0 / 255.0f blue:29.0 / 255.0f alpha:1];
}

+ (UIColor *)blueTextColor {
    return [UIColor colorWithRed: 20.0 / 255.0f green:115.0 / 255.0f blue:232.0 / 255.0f alpha:1];
}

+ (UIColor *)deepDarkGrayTextColor {
    return [UIColor colorWithRed: 51.0 / 255.0f green:51.0 / 255.0f blue:51.0 / 255.0f alpha:1];
}

+ (UIColor *)darkGrayTextColor {
    return [UIColor colorWithRed: 136.0 / 255.0f green:136.0 / 255.0f blue:136.0 / 255.0f alpha:1];
}

+ (UIColor *)middleGrayTextColor {
    return [UIColor colorWithRed: 191.0 / 255.0f green:191.0 / 255.0f blue:191.0 / 255.0f alpha:1];
}

+ (UIColor *)lightGrayTextColor {
    return [UIColor colorWithRed: 239.0 / 255.0f green:239.0 / 255.0f blue:244.0 / 255.0f alpha:1];
}

+ (UIColor *)borderColor {
    return [UIColor colorWithHexString:@"#C8C7CC"];
}


#pragma mark -  Custom Color
+ (UIColor *)colorWithHex:(long)hexRGB {
    return [UIColor colorWithHex:hexRGB alpha:1];
}

+ (UIColor *)colorWithHex:(long)hexRGB alpha:(int)alpha {
    if (hexRGB <= 0xFF) {
        return [UIColor colorWithRed:hexRGB / 255.0f
                               green:hexRGB / 255.0f
                                blue:hexRGB / 255.0f
                               alpha:alpha];
    } else {
        return [UIColor colorWithRed:(((hexRGB & 0xFF0000) >> 16))/255.0f
                               green:(((hexRGB & 0xFF00) >> 8))/255.0f
                                blue:((hexRGB & 0xFF))/255.0f
                               alpha:alpha];
    }
}

+ (UIColor *)colorWithHexString:(NSString *)hexColor {
    hexColor = [hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([hexColor length] < 6) {
        return nil;
    }
    
    if ([hexColor hasPrefix:@"#"]) {
        hexColor = [hexColor substringFromIndex:1];
    }
    
    NSRange range = NSMakeRange(0, 2);
    NSString *rs = [hexColor substringWithRange:range];
    range.location = 2;
    NSString *gs = [hexColor substringWithRange:range];
    range.location = 4;
    NSString *bs = [hexColor substringWithRange:range];
    
    unsigned int r, g, b, a;
    [[NSScanner scannerWithString:rs] scanHexInt:&r];
    [[NSScanner scannerWithString:gs] scanHexInt:&g];
    [[NSScanner scannerWithString:bs] scanHexInt:&b];
    
    if ([hexColor length] == 8) {
        range.location = 4;
        NSString *as = [hexColor substringWithRange:range];
        [[NSScanner scannerWithString:as] scanHexInt:&a];
    } else {
        a = 255;
    }
    
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:((float)a / 255.0f)];
}



@end
