//
//  UIFont+Category.m
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/8.
//
//

#import "UIFont+Category.h"

#define kScreenW                [UIScreen mainScreen].bounds.size.width
#define kGlobalFontName         @"HelveticaNeue-Light"
#define kGlobalMediumFontName   @"HelveticaNeue-Medium"
#define kGlobalBoldFontName     @"HelveticaNeue-Bold"
#define iPhone_6                375
#define iPhone_6Plus            414


@implementation UIFont (Category)

+ (UIFont *)titleFont {
    return [UIFont fontWithName:kGlobalFontName size:20];
}

+ (UIFont *)adaptSystemFontWithStandarSize:(NSInteger)standardSize {
    NSInteger scaleFont = kScreenW >= iPhone_6Plus ? standardSize : floor( kScreenW / iPhone_6 * standardSize);
    return  [UIFont systemFontOfSize:scaleFont];
}

+ (UIFont *)adaptBoldFontWithStandardSize:(NSInteger)standardSize {
    NSInteger scaleFont = kScreenW >= iPhone_6Plus ? standardSize : floor(kScreenW / iPhone_6 * standardSize);
    return  [UIFont boldSystemFontOfSize:scaleFont];
}

+ (UIFont *)sizeFontWithSystem:(float)size {
    return [UIFont fontWithName:kGlobalFontName size:size];
}

+ (UIFont*)sizeFontWithMedium:(float)size {
    return [UIFont fontWithName:kGlobalMediumFontName size:size];
}

+ (UIFont *)sizeFontWithBold:(float)size {
    return [UIFont fontWithName:kGlobalBoldFontName size:size];
}

+ (UIFont *)sizeFontWithItalic:(float)size {
    CGAffineTransform matrix = CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
    UIFontDescriptor *desc = [UIFontDescriptor fontDescriptorWithName:kGlobalFontName
                                                               matrix:matrix];
    return [UIFont fontWithDescriptor:desc size:size];
}

+ (UIFont *)sizeFontWithMediumItalic:(float)size {
    CGAffineTransform matrix = CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
    UIFontDescriptor *desc = [UIFontDescriptor fontDescriptorWithName:kGlobalMediumFontName
                                                               matrix:matrix];
    return [UIFont fontWithDescriptor:desc size:size];
}

+ (UIFont *)sizeFontWithBoldItalic:(float)size {
    CGAffineTransform matrix = CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
    UIFontDescriptor *desc = [UIFontDescriptor fontDescriptorWithName:kGlobalBoldFontName
                                                               matrix:matrix];
    return [UIFont fontWithDescriptor:desc size:size];
}

@end
