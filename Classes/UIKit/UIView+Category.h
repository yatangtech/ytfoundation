//
//  UIView+Category.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/9.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Category)
@property (nullable, nonatomic, readonly) UIViewController *viewController;

///< Shortcut for frame.origin.x.
@property (nonatomic) CGFloat left;

///< Shortcut for frame.origin.y
@property (nonatomic) CGFloat top;

///< Shortcut for frame.origin.x + frame.size.width
@property (nonatomic) CGFloat right;

///< Shortcut for frame.origin.y + frame.size.height
@property (nonatomic) CGFloat bottom;

///< Shortcut for frame.size.width.
@property (nonatomic) CGFloat width;

///< Shortcut for frame.size.height.
@property (nonatomic) CGFloat height;

///< Shortcut for center.x
@property (nonatomic) CGFloat centerX;

///< Shortcut for center.y
@property (nonatomic) CGFloat centerY;

///< Shortcut for frame.origin.
@property (nonatomic) CGPoint origin;

///< Shortcut for frame.size.
@property (nonatomic) CGSize  size;

/// 可视化边框圆角
@property(assign, nonatomic) IBInspectable NSInteger cornerRadius;
/// 可视化边框宽度
@property(assign, nonatomic) IBInspectable CGFloat borderWidth;
/// 可视化边框颜色
@property(strong, nonatomic, nonnull) IBInspectable UIColor * borderColor;

#pragma mark -  配置方法
/**
 * 从 XIB 获取 view 实例对象
 */
+ (nonnull instancetype)viewInXib;

/**
 * 从 bundle 中获取 view 对象
 */
+ (nullable UINib *)viewNib;

/**
 * 注册唯一标识
 */
+ (nonnull NSString*)identifier;

#pragma mark - Custom Method
/**
 * 截图
 * 返回值：UIImage
 */
- (nullable UIImage *)screenShot;

/**
 * 获取第一响应者
 * 返回： UIView
 */
- (nullable UIView *)firstResponder;

/**
 * 查找并注册第一响应者
 * 返回： BOOL
 */
- (BOOL)findAndResignFirstResponder;


/**
 * 移除 view 上所有子视图
 */
- (void)removeAllSubviews;

@end
