//
//  UIFont+Category.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/8.
//
//

#import <UIKit/UIKit.h>

@interface UIFont (Category)

/**
 * 标题字号 默认为20号 系统字体
 * 参数： 无
 * 返回值：UIFont
 */

+ (UIFont *)titleFont;

/**
 * 根据不同分辨率适配字体大小
 * standardSize : 字体大小
 * 返回值：UIFont
 */

+ (UIFont *)adaptSystemFontWithStandarSize:(NSInteger)standardSize;

/**
 * 根据不同分辨率适配粗体字大小
 * standardSize : 字体大小
 * 返回值：UIFont
 */

+ (UIFont *)adaptBoldFontWithStandardSize:(NSInteger)standardSize;

/**
 * 设置指定字号的系统字体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithSystem:(float)size;

/**
 * 设置指定字号的中型字体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithMedium:(float)size;

/**
 * 设置指定字号的粗体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithBold:(float)size;

/**
 * 设置指定字号的斜体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithItalic:(float)size;

/**
 * 设置指定字号的中型斜体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithMediumItalic:(float)size;

/**
 * 设置指定字号的加粗斜体
 * 参数：字号
 * 返回值：UIFont
 */

+ (UIFont *)sizeFontWithBoldItalic:(float)size;


@end
