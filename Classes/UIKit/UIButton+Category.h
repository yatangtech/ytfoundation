//
//  UIButton+Category.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/9.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    ButtonStyleRed,
    ButtonStyleWhite,
    ButtonStyleLightGray,
    ButtonStyleMiddleGray,
    ButtonStyleDeepDarkGray,
    ButtonStyleDarkGray,
    ButtonStyleBlue,
} ButtonStyle;


@interface UIButton (Category)

/**
 * 设置指定状态下的按钮背景色
 * 参数：按钮状态
 * 返回值：无
 */

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;

/**
 * 根据按钮标题和按钮风格定制按钮
 * 参数1： 按钮标题
 * 参数2： 按钮风格
 * 返回值： UIButton
 */
+ (UIButton *)buttonWithTitle:(NSString *)title buttonStyle:(ButtonStyle)buttonStyle;


/**
 * 设置按钮标题风格
 * 参数：标题风格 枚举类型
 * 返回值： 无
 */
- (void)setTitleStyle:(ButtonStyle)buttonStyle;

/**
 * 设置含有透明度按钮标题风格
 * 参数1： 按钮标题风格
 * 参数2： 透明度
 * 返回值： 无
 */
- (void)setTitleStyle:(ButtonStyle)buttonStyle normalAlpha:(CGFloat)alpha;

#pragma mark - 无边框按钮
/**
 * 设置按钮风格
 * 参数：按钮风格 枚举
 * 返回值：无
 */
- (void)setSolidStyle:(ButtonStyle)buttonStyle;

/**
 * 设置带圆角的按钮
 * 参数1：按钮风格
 * 参数2： 圆角
 * 返回值：无
 */
- (void)setSolidStyle:(ButtonStyle)buttonStyle radius:(float)radius;

/**
 * 设置带圆角到透明度的按钮
 * 参数1：按钮风格
 * 参数2：圆角
 * 参数3：透明度
 */
- (void)setSolidStyle:(ButtonStyle)buttonStyle
               radius:(float)radius
          normalAlpha:(CGFloat)alpha;


#pragma mark - 有边框按钮
/**
 * 设置按钮边框风格
 * 参数：按钮风格 枚举
 * 返回值： 无
 */
- (void)setBorderStyle:(ButtonStyle)buttonStyle;


/**
 * 设置按钮边框风格及圆角
 * 参数1: 按钮风格
 * 参数2: 圆角度数
 * 返回值: 无
 */
- (void)setBorderStyle:(ButtonStyle)buttonStyle radius:(float)radius;

/**
 * 设置边框风格及圆角及边框透明度
 * 参数1: 按钮风格
 * 参数2: 圆角度数
 * 参数3: 透明度
 * 返回值: 无
 */
- (void)setBorderStyle:(ButtonStyle)buttonStyle
                radius:(float)radius
           borderAlpha:(CGFloat)alpha;

/**
 * 设置具有间距的按钮图片及标题
 * 参数： 间距
 * 返回值： 无
 */
- (void)centerImageAndTitle:(float)space;

/**
 * 设置无间距按钮图片及标题  默认间距为 5
 * 参数：无
 * 返回值： 无
 */
- (void)centerImageAndTitle;


/**
 * 反转按钮图片和标题的位置 image在右 title在左
 * 参数： 无
 * 返回值： 无
 */
- (void)reverseButtonImage;



@end
