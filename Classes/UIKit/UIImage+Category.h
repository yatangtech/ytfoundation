//
//  UIImage+Category.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/9.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)

/*
 * 生成指定颜色指定大小的图片
 * 参数：color
 * 返回值： UIImage
 */

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

/*
 * 生成指定颜色的图片
 * 参数：color
 * 返回值： UIImage
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 * 根据alpha值生成图片
 *
 */
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha;

@end
