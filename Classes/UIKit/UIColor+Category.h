//
//  UIColor+Category.h
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/8.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Category)

#pragma mark - Application Color

+ (UIColor *)redTextColor;

+ (UIColor *)blueTextColor;

+ (UIColor *)deepDarkGrayTextColor;

+ (UIColor *)darkGrayTextColor;

+ (UIColor *)middleGrayTextColor;

+ (UIColor *)lightGrayTextColor;

+ (UIColor *)borderColor;

#pragma mark - Custom Color

/**
 * 十六进制数值 不带透明度 透明度默认为 1
 * 参数：16进制数
 * 返回：UIColor
 */

+ (UIColor *)colorWithHex:(long)hexRGB;

/**
 * 十六进制数值 带透明度
 * 参数：16进制数、透明度
 * 返回：UIColor
 */

+ (UIColor *)colorWithHex:(long)hexRGB alpha:(int)alpha;

/**
 * 根据十六进制字符串设置颜色
 * 参数：16进制字符串
 * 返回：UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)hexColor;


@end
