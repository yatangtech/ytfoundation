//
//  UIButton+Category.m
//  Pods
//
//  Created by Jeavil_Tang on 2017/5/9.
//
//

#import "UIButton+Category.h"
#import "UIColor+Category.h"
#import "UIImage+Category.h"

#define SystemSize 17

@implementation UIButton (Category)

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIImage imageWithColor:backgroundColor] forState:state];
}

+ (id)buttonWithTitle:(NSString *)title buttonStyle:(ButtonStyle)buttonStyle {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setSolidStyle:buttonStyle];
    
    button.titleLabel.font = [UIFont systemFontOfSize:SystemSize];
    
    return button;
}

- (void)setTitleStyle:(ButtonStyle)buttonStyle {
    [self setTitleStyle:buttonStyle normalAlpha:1];
}

- (void)setTitleStyle:(ButtonStyle)buttonStyle normalAlpha:(CGFloat)alpha {
    switch (buttonStyle) {
        case ButtonStyleRed:
            [self setTitleColor:[[UIColor redTextColor] colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self setTitleColor:[[UIColor redTextColor] colorWithAlphaComponent:.6]
                       forState:UIControlStateHighlighted];
            [self setTitleColor:[[UIColor redTextColor] colorWithAlphaComponent:.4]
                       forState:UIControlStateDisabled];
            break;
        case ButtonStyleBlue:
            [self setTitleColor:[[UIColor blueTextColor] colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self setTitleColor:[[UIColor blueTextColor] colorWithAlphaComponent:.6]
                       forState:UIControlStateHighlighted];
            [self setTitleColor:[[UIColor blueTextColor] colorWithAlphaComponent:.4]
                       forState:UIControlStateDisabled];
            break;
        case ButtonStyleDeepDarkGray:
            [self setTitleColor:[[UIColor deepDarkGrayTextColor]
                                 colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self
             setTitleColor:[[UIColor deepDarkGrayTextColor] colorWithAlphaComponent:.6]
             forState:UIControlStateHighlighted];
            [self
             setTitleColor:[[UIColor deepDarkGrayTextColor] colorWithAlphaComponent:.4]
             forState:UIControlStateDisabled];
            break;
        case ButtonStyleLightGray:
            [self setTitleColor:[[UIColor lightGrayTextColor]
                                 colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self
             setTitleColor:[[UIColor lightGrayTextColor] colorWithAlphaComponent:.6]
             forState:UIControlStateHighlighted];
            [self
             setTitleColor:[[UIColor lightGrayTextColor] colorWithAlphaComponent:.4]
             forState:UIControlStateDisabled];
            break;
        case ButtonStyleMiddleGray:
            [self setTitleColor:[[UIColor middleGrayTextColor]
                                 colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self
             setTitleColor:[[UIColor middleGrayTextColor] colorWithAlphaComponent:.6]
             forState:UIControlStateHighlighted];
            [self
             setTitleColor:[[UIColor middleGrayTextColor] colorWithAlphaComponent:.4]
             forState:UIControlStateDisabled];
            break;
        case ButtonStyleDarkGray:
            [self setTitleColor:[[UIColor darkGrayTextColor]
                                 colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self setTitleColor:[[UIColor darkGrayTextColor] colorWithAlphaComponent:.6]
                       forState:UIControlStateHighlighted];
            [self setTitleColor:[[UIColor darkGrayTextColor] colorWithAlphaComponent:.4]
                       forState:UIControlStateDisabled];
            break;
        case ButtonStyleWhite:
            [self setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]
                       forState:UIControlStateNormal];
            [self setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:.6]
                       forState:UIControlStateHighlighted];
            [self setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:.4]
                       forState:UIControlStateDisabled];
            break;
    }
}


#pragma mark - 无边框按钮

- (void)setSolidStyle:(ButtonStyle)buttonStyle {
    [self setSolidStyle:buttonStyle radius:0];
}

- (void)setSolidStyle:(ButtonStyle)buttonStyle radius:(float)radius {
    [self setSolidStyle:buttonStyle radius:radius normalAlpha:1];
}

- (void)setSolidStyle:(ButtonStyle)buttonStyle
               radius:(float)radius
          normalAlpha:(CGFloat)alpha {
    switch (buttonStyle) {
        case ButtonStyleRed:
            [self setBackgroundImage:
            [UIImage imageWithColor:[[UIColor redTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor redTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor redTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;
            
        case ButtonStyleBlue:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor blueTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor blueTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor blueTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;

        case ButtonStyleDeepDarkGray:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor deepDarkGrayTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor deepDarkGrayTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor deepDarkGrayTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;


       case ButtonStyleLightGray:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor lightGrayTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor lightGrayTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor lightGrayTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;
        case ButtonStyleMiddleGray:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor middleGrayTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor middleGrayTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor middleGrayTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;
        case ButtonStyleDarkGray:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor darkGrayTextColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor darkGrayTextColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor darkGrayTextColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;
            
        case ButtonStyleWhite:
            [self setBackgroundImage:
             [UIImage imageWithColor:[[UIColor whiteColor]
                                      colorWithAlphaComponent:alpha]]
                            forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor whiteColor]
                                                      colorWithAlphaComponent:.6]]
                            forState:UIControlStateHighlighted];
            [self setBackgroundImage:[UIImage
                                      imageWithColor:[[UIColor whiteColor]
                                                      colorWithAlphaComponent:.4]]
                            forState:UIControlStateDisabled];
            break;
    }
    
    self.layer.borderWidth = 0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithWhite:1 alpha:.6]
               forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor colorWithWhite:1 alpha:.4]
               forState:UIControlStateDisabled];
    [self setClipsToBounds:YES];
    [self.layer setCornerRadius:radius];
}

#pragma mark - 带边框按钮

- (void)setBorderStyle:(ButtonStyle)buttonStyle {
    [self setBorderStyle:buttonStyle radius:0];
}

- (void)setBorderStyle:(ButtonStyle)buttonStyle radius:(float)radius {
    [self setBorderStyle:buttonStyle radius:radius borderAlpha:1];
}

- (void)setBorderStyle:(ButtonStyle)buttonStyle
                radius:(float)radius
           borderAlpha:(CGFloat)alpha {
    UIColor *borderColor = nil;
    UIColor *titleColor = nil;
    UIColor *highlightedColor = nil;
    
    switch (buttonStyle) {
        case ButtonStyleRed:
            highlightedColor = [UIColor redTextColor];
            titleColor = [[UIColor redTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
        case ButtonStyleBlue:
            highlightedColor = [UIColor blueTextColor];
            titleColor = [[UIColor blueTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
        case ButtonStyleDeepDarkGray:
            highlightedColor = [UIColor deepDarkGrayTextColor];
            titleColor = [[UIColor deepDarkGrayTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;

        case ButtonStyleLightGray:
            highlightedColor = [UIColor lightGrayTextColor];
            titleColor = [[UIColor lightGrayTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
        case ButtonStyleMiddleGray:
            highlightedColor = [UIColor middleGrayTextColor];
            titleColor = [[UIColor middleGrayTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
        case ButtonStyleDarkGray:
            highlightedColor = [UIColor darkGrayTextColor];
            titleColor = [[UIColor darkGrayTextColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
        case ButtonStyleWhite:
            highlightedColor = [UIColor whiteColor];
            titleColor = [[UIColor whiteColor] colorWithAlphaComponent:alpha];
            borderColor = titleColor;
            break;
    }
    
    [self setTitleColor:buttonStyle != ButtonStyleWhite
                       ? [UIColor whiteColor]
                       : [UIColor middleGrayTextColor]
               forState:UIControlStateHighlighted];
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = radius;
    
    self.layer.borderColor = borderColor.CGColor;
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageWithColor:highlightedColor]
                    forState:UIControlStateHighlighted];
    
    [self setBackgroundImage:[[UIImage alloc] init]
                    forState:UIControlStateNormal];
}


- (void)centerImageAndTitle:(float)spacing {
    // get the size of the elements here for readability
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = [[self titleForState:UIControlStateNormal]
                        sizeWithAttributes:@{NSFontAttributeName : self.titleLabel.font}];
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    
    // raise the image and push it right to center it
    self.imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight - imageSize.height),
                                            0.0, 0.0, -titleSize.width);
    
    // lower the text and push it left to center it
    self.titleEdgeInsets = UIEdgeInsetsMake(
                                            0.0, -imageSize.width, -(totalHeight - titleSize.height), 0.0);
}

- (void)centerImageAndTitle {
    const int DEFAULT_SPACING = 5.0f;
    [self centerImageAndTitle:DEFAULT_SPACING];
}


- (void)reverseButtonImage {
    self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
}

@end
