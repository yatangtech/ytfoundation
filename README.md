# YTFoundation

[![CI Status](http://img.shields.io/travis/lixiang/YTFoundation.svg?style=flat)](https://travis-ci.org/lixiang/YTFoundation)
[![Version](https://img.shields.io/cocoapods/v/YTFoundation.svg?style=flat)](http://cocoapods.org/pods/YTFoundation)
[![License](https://img.shields.io/cocoapods/l/YTFoundation.svg?style=flat)](http://cocoapods.org/pods/YTFoundation)
[![Platform](https://img.shields.io/cocoapods/p/YTFoundation.svg?style=flat)](http://cocoapods.org/pods/YTFoundation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTFoundation is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YTFoundation"
```

## Author

lixiang, lixiang@yatang.cn

## License

YTFoundation is available under the MIT license. See the LICENSE file for more info.
