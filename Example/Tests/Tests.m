//
//  YTFoundationTests.m
//  YTFoundationTests
//
//  Created by lixiang on 05/02/2017.
//  Copyright (c) 2017 lixiang. All rights reserved.
//

@import XCTest;
#import "YTViewController.h"

@interface Tests : XCTestCase
@property (nonatomic, strong) YTViewController *vc;
@end

@implementation Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.vc = [[YTViewController alloc] init];
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.vc = nil;
    [super tearDown];
}


- (void)test_fontcatagory {
//    [self.vc testFont];
    
    [self.vc testButton];
}

- (void)testExample
{
    //    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end

