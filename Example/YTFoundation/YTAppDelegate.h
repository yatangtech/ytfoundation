//
//  YTAppDelegate.h
//  YTFoundation
//
//  Created by lixiang on 05/02/2017.
//  Copyright (c) 2017 lixiang. All rights reserved.
//

@import UIKit;

@interface YTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
