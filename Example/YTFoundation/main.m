//
//  main.m
//  YTFoundation
//
//  Created by lixiang on 05/02/2017.
//  Copyright (c) 2017 lixiang. All rights reserved.
//

@import UIKit;
#import "YTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YTAppDelegate class]));
    }
}
