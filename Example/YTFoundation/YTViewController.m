//
//  YTViewController.m
//  YTFoundation
//
//  Created by lixiang on 05/02/2017.
//  Copyright (c) 2017 lixiang. All rights reserved.
//

#import "YTViewController.h"
#import <YTFoundation/YTFoundation.h>

@interface YTViewController ()

@end

@implementation YTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//    [self testFont];
    [self testButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)testButton {
    UIButton *button = [UIButton new];
    button.frame = CGRectMake(0, 0, 200, 30);
    button.center = CGPointMake(CGRectGetMidX(self.view.bounds), 40);
    [button setBackgroundColor:[UIColor blueTextColor] forState:UIControlStateNormal];
    [button setTitle:@"设置背景色" forState:UIControlStateNormal];
    
    UIButton *button1 = [UIButton buttonWithTitle:@"设置标题 & 风格" buttonStyle:ButtonStyleRed];
    button1.frame = CGRectMake(0, 0, 200, 30);
    button1.center = CGPointMake(CGRectGetMidX(self.view.bounds), 80);
    [button1 setSolidStyle:ButtonStyleRed radius:8 normalAlpha:1];

   
    UIButton *button2 = [UIButton new];
    button2.frame = CGRectMake(0, 0, 200, 30);
    button2.center = CGPointMake(CGRectGetMidX(self.view.bounds), 120);
    [button2 setTitle:@"设置标题风格" forState:UIControlStateNormal];
    [button2 setTitleStyle:ButtonStyleWhite normalAlpha:1];
    [button2 setSolidStyle:ButtonStyleDeepDarkGray radius:8 normalAlpha:1];
    
    UIButton *button3 = [UIButton new];
    button3.frame = CGRectMake(0, 0, 200, 30);
    button3.center = CGPointMake(CGRectGetMidX(self.view.bounds), 160);
    [button3 setTitle:@"设置无边框button" forState:UIControlStateNormal];
    [button3 setTitleStyle:ButtonStyleWhite normalAlpha:1];
    [button3 setSolidStyle:ButtonStyleBlue radius:8 normalAlpha:.7];
    
    UIButton *button4 = [UIButton new];
    button4.frame = CGRectMake(0, 0, 200, 30);
    button4.center = CGPointMake(CGRectGetMidX(self.view.bounds), 200);
    [button4 setTitle:@"设置有边框button" forState:UIControlStateNormal];
    [button4 setTitleStyle:ButtonStyleWhite normalAlpha:1];
    [button4 setBorderStyle:ButtonStyleDarkGray radius:8 borderAlpha:1];

    UIButton *button5 = [UIButton new];
    button5.frame = CGRectMake(0, 0, 200, 60);
    button5.center = CGPointMake(CGRectGetMidX(self.view.bounds), 260);
    [button5 setTitle:@"title & image" forState:UIControlStateNormal];
    [button5 setImage:[UIImage imageNamed:@"Login_account@2x.png"] forState:UIControlStateNormal];
    button5.imageView.contentMode = UIViewContentModeTop;
    [button5 setTitleStyle:ButtonStyleWhite normalAlpha:1];
    [button5 setBorderStyle:ButtonStyleRed radius:8 borderAlpha:1];
    [button5 centerImageAndTitle:10];
    
    UIButton *button6 = [UIButton new];
    button6.frame = CGRectMake(0, 0, 200, 60);
    button6.center = CGPointMake(CGRectGetMidX(self.view.bounds), 340);
    [button6 setTitle:@"reverse  " forState:UIControlStateNormal];
    [button6 setImage:[UIImage imageNamed:@"Login_account@2x.png"] forState:UIControlStateNormal];
    button6.imageView.contentMode = UIViewContentModeTop;
    [button6 setTitleStyle:ButtonStyleWhite normalAlpha:1];
    [button6 setBorderStyle:ButtonStyleBlue radius:8 borderAlpha:1];
    [button6 reverseButtonImage];


    [self.view addSubview:button];
    [self.view addSubview:button1];
    [self.view addSubview:button2];
    [self.view addSubview:button3];
    [self.view addSubview:button4];
    [self.view addSubview:button5];
    [self.view addSubview:button6];
}

    
    
- (void)testFont {
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(0, 0, 200, 30);
    label.center = CGPointMake(CGRectGetMidX(self.view.bounds), 40);
    label.text = @"标题字体";
    label.font = [UIFont titleFont];
    label.backgroundColor = [UIColor redTextColor];
    label.textColor = [UIColor whiteColor];
    
    UILabel *label1 = [UILabel new];
    label1.frame = CGRectMake(0, 0, 200, 30);
    label1.center = CGPointMake(CGRectGetMidX(self.view.bounds), 80);
    label1.text = @"自定义系统字体";
    label1.font = [UIFont sizeFontWithSystem:25];
    label1.backgroundColor = [UIColor deepDarkGrayTextColor];
    label1.textColor = [UIColor whiteColor];
    
    UILabel *label2 = [UILabel new];
    label2.frame = CGRectMake(0, 0, 200, 30);
    label2.center = CGPointMake(CGRectGetMidX(self.view.bounds),  120);
    label2.text = @"自定义中号字体";
    label2.font = [UIFont sizeFontWithMedium:20];
    label2.backgroundColor = [UIColor middleGrayTextColor];
    label2.textColor = [UIColor whiteColor];
    
    UILabel *label3 = [UILabel new];
    label3.frame = CGRectMake(0, 0, 200, 30);
    label3.center = CGPointMake(CGRectGetMidX(self.view.bounds),  160);
    label3.text = @"自定义粗体";
    label3.font = [UIFont sizeFontWithBold:20];
    label3.backgroundColor = [UIColor darkGrayTextColor];
    label3.textColor = [UIColor whiteColor];
    
    UILabel *label4 = [UILabel new];
    label4.frame = CGRectMake(0, 0, 200, 30);
    label4.center = CGPointMake(CGRectGetMidX(self.view.bounds),  200);
    label4.text = @"自定义斜体";
    label4.font = [UIFont sizeFontWithItalic:20];
    label4.backgroundColor = [UIColor lightGrayTextColor];
    label4.textColor = [UIColor whiteColor];
    
    UILabel *label5 = [UILabel new];
    label5.frame = CGRectMake(0, 0, 200, 30);
    label5.center = CGPointMake(CGRectGetMidX(self.view.bounds),  240);
    label5.text = @"自定义中型斜体";
    label5.font = [UIFont sizeFontWithMediumItalic:20];
    label5.backgroundColor = [UIColor blueTextColor];
    label5.textColor = [UIColor whiteColor];
    
    UILabel *label6 = [UILabel new];
    label6.frame = CGRectMake(0, 0, 200, 30);
    label6.center = CGPointMake(CGRectGetMidX(self.view.bounds),  280);
    label6.text = @"自定义加粗斜体";
    label6.font = [UIFont sizeFontWithBoldItalic:20];
    label6.backgroundColor = [UIColor borderColor];
    label6.textColor = [UIColor whiteColor];


    
    [self.view addSubview:label];
    [self.view addSubview:label1];
    [self.view addSubview:label2];
    [self.view addSubview:label3];
    [self.view addSubview:label4];
    [self.view addSubview:label5];
    [self.view addSubview:label6];

    
    NSLog(@"----->> label = %@", label.font);
    NSLog(@"----->> label1 = %@", label1.font);
    NSLog(@"----->> label2 = %@", label2.font);
    NSLog(@"----->> label3 = %@", label3.font);
    NSLog(@"----->> label4 = %@", label4.font);
    NSLog(@"----->> label5 = %@", label5.font);
    NSLog(@"----->> label6 = %@", label6.font);



}

@end
